package com.darmajaya.petshop.adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.darmajaya.petshop.R;
import com.darmajaya.petshop.utils.DetailModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Parsania Hardik on 23/04/2016.
 */
public class Definition_Adapter extends PagerAdapter {


    private ArrayList<DetailModel> detailModelArrayList;
    private LayoutInflater inflater;
    private Context context;

    public Definition_Adapter(Context context, ArrayList<DetailModel> detailModelArrayList) {
        this.context = context;
        this.detailModelArrayList = detailModelArrayList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return detailModelArrayList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.detailpetshop, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.foto);
        System.out.println(detailModelArrayList.get(position).getImage_drawable()+".jpg");

        Picasso.get().load("file:///android_asset/"+detailModelArrayList.get(position).getImage_drawable()+".jpg").into(imageView);

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}