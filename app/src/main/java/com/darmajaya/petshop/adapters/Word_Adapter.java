package com.darmajaya.petshop.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.darmajaya.petshop.DetailPetshop;
import com.darmajaya.petshop.R;
import com.darmajaya.petshop.utils.TokoModel;

import java.util.List;

/**
 * Created by Kurnia on 12/20/2017.
 */


public class Word_Adapter extends RecyclerView.Adapter<Word_Adapter.ViewHolder> {

    public List<TokoModel> data;

    public Word_Adapter(){}
    public void setData(List<TokoModel> data){
        this.data=data;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context=parent.getContext();
        LayoutInflater inflater=LayoutInflater.from(context);

        View wordView=inflater.inflate(R.layout.word_item,parent,false);

        ViewHolder viewHolder=new ViewHolder(wordView,context);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TokoModel TokoModel=data.get(position);
        holder.wordText.setText(TokoModel.getNamatoko());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public Context context;
        public TextView wordText;
        public ViewHolder(View itemView, final Context context) {
            super(itemView);
            this.context=context;
            wordText=(TextView) itemView.findViewById(R.id.namatokotext);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position=getAdapterPosition();
                    TokoModel TokoModel=data.get(position);

                    Intent intent=new Intent(context, DetailPetshop.class);
                    intent.putExtra("NAMATOKO",TokoModel.getNamatoko());
                    context.startActivity(intent);
                }
            });
        }
    }
}
