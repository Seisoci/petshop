package com.darmajaya.petshop;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.darmajaya.petshop.adapters.Definition_Adapter;
import com.darmajaya.petshop.utils.DatabaseHelper;
import com.darmajaya.petshop.utils.DetailModel;
import com.darmajaya.petshop.utils.ImageModel;
import com.darmajaya.petshop.utils.Toko;
import com.darmajaya.petshop.utils.TokoModel;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;


public class DetailPetshop extends AppCompatActivity {
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private ArrayList<DetailModel> detailModelArrayList;
    public String arraylist;
    private DatabaseHelper database;
    String namatoko, gambar, daerah, alamat, deskripsi, notelp;


    private String [] imagetempat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sliderdetail);


        String toko = getIntent().getStringExtra("NAMATOKO");


        database = new DatabaseHelper(DetailPetshop.this);
        final SQLiteDatabase db = database.getReadableDatabase();
        List<TokoModel> data;
        data= database.getall();

        for (TokoModel p : data) {
             namatoko = p.getNamatoko();

             if (namatoko.equals(toko)){
                 gambar= p.getGambar();
                 daerah = p.getDaerah();
                 alamat = p.getAlamat();
                 deskripsi = p.getDeskripsi();
                 notelp = p.getNotelp();

             }


        }


        TextView tokotext = (TextView) findViewById(R.id.namatokotext);
//        TextView daerahtext = (TextView) findViewById(R.id.daerahtext);
        TextView alamattext = (TextView) findViewById(R.id.alamattext);
        TextView deskripsitext = (TextView) findViewById(R.id.deskripsitext);
        TextView notelptext = (TextView) findViewById(R.id.notelp);


        tokotext.setText(toko);
        alamattext.setText("Alamat: "+ alamat);
        deskripsitext.setText("Deskripsi: " + '\n' +deskripsi);
        notelptext.setText(notelp);

        notelptext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(DetailPetshop.this);
                builder1.setMessage("Hubungi Toko Pet Shop ini?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Ya",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialPhoneNumber(notelp);
                            }
                        });

                builder1.setNegativeButton(
                        "Tidak",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

            }
        });

        arraylist = gambar;
        imagetempat = arraylist.split(",");
        detailModelArrayList = new ArrayList<>();
        detailModelArrayList = populateList();
        init();
    }

    public void dialPhoneNumber(String notelp) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + notelp));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }



        private ArrayList<DetailModel> populateList(){

            ArrayList<DetailModel> list = new ArrayList<>();

            for(int i = 0; i < imagetempat.length; i++){
                DetailModel imageModel = new DetailModel();
                imageModel.setImage_drawable(imagetempat[i].trim());
                list.add(imageModel);
        }
        return list;
        }


    private void init() {

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new Definition_Adapter(DetailPetshop.this,detailModelArrayList));

        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        //Set circle indicator radius
        indicator.setRadius(4 * density);

        NUM_PAGES =detailModelArrayList.size();


        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if(id==android.R.id.home){
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
