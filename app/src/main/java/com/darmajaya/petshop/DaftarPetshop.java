package com.darmajaya.petshop;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.darmajaya.petshop.utils.PetShop;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import es.dmoral.toasty.Toasty;

public class DaftarPetshop extends AppCompatActivity {
    private DatabaseReference databaseReference;
    private EditText namapetshop, notelpps, alamatps, deskripsi, user, usertelp, useremail;
    private Button daftar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_petshop);

        namapetshop = (EditText) findViewById(R.id.namapetshop);
        notelpps = (EditText) findViewById(R.id.notelppetshop);
        alamatps = (EditText) findViewById(R.id.alamatpetshop);
        deskripsi = (EditText) findViewById(R.id.deskripsipetshop);
        user = (EditText) findViewById(R.id.user);
        usertelp = (EditText) findViewById(R.id.usernotelp);
        useremail = (EditText) findViewById(R.id.useremail);
        daftar = (Button) findViewById(R.id.submit);

        databaseReference = FirebaseDatabase.getInstance().getReference("Petshop");
        daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String namatoko = namapetshop.getText().toString();
                String notelptoko = notelpps.getText().toString();
                String alamattoko = alamatps.getText().toString();
                String deskripsitoko = deskripsi.getText().toString();
                String nama = user.getText().toString();
                String notelp = usertelp.getText().toString();
                String email = useremail.getText().toString();

                if(!TextUtils.isEmpty(namatoko) && !TextUtils.isEmpty(notelptoko)
                        && !TextUtils.isEmpty(alamattoko)
                        && !TextUtils.isEmpty(deskripsitoko)
                        && !TextUtils.isEmpty(nama)
                        && !TextUtils.isEmpty(notelp)
                        && !TextUtils.isEmpty(email)) {

                    PetShop petshop = new PetShop();
                    petshop.setNamapetshop(namapetshop.getText().toString());
                    petshop.setNotelpps(notelpps.getText().toString());
                    petshop.setAlamatps(alamatps.getText().toString());
                    petshop.setDeskripsi(deskripsi.getText().toString());
                    petshop.setNameuser(user.getText().toString());
                    petshop.setNotelpuser(usertelp.getText().toString());
                    petshop.setUseremail(useremail.getText().toString());
                    String idgenerate = databaseReference.push().getKey();
                    databaseReference.child(idgenerate).setValue(petshop).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            finish();
                            Toasty.custom(DaftarPetshop.this, "Pendaftaran Sukses", getResources().getDrawable(R.drawable.ic_pet), Color.parseColor("#00C853"),
                                    Toast.LENGTH_LONG, true, true).show();
                            Intent intent = new Intent(DaftarPetshop.this, MainActivity.class);
                            startActivity(intent);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toasty.warning(DaftarPetshop.this, "Pastikan koneksi sudah terhubung internet", Toast.LENGTH_LONG, true).show();
                        }
                    });
                }
                else
                {
                    Toasty.error(DaftarPetshop.this, "Tolong di isi dengan lengkap", Toast.LENGTH_LONG, true).show();
                }
            }
        });
    }

}
