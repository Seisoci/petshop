package com.darmajaya.petshop.utils;

/**
 * Created by Kurnia on 12/20/2017.
 */


public class Toko {

    private String id;
    private String namatoko;
    private String notelp;
    private String deskripsi;
    private String alamat;
    private String daerah;
    private String gambar;
    private String koordinat;
    private String estimasi;

    public Toko(String id, String namatoko, String notelp, String deskripsi, String alamat, String daerah, String gambar, String koordinat, String estimasi) {
        this.id = id;
        this.namatoko = namatoko;
        this.notelp = notelp;
        this.deskripsi = deskripsi;
        this.alamat = alamat;
        this.daerah = daerah;
        this.gambar = gambar;
        this.koordinat = koordinat;
        this.estimasi = estimasi;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNamatoko() {
        return namatoko;
    }

    public void setNamatoko(String namatoko) {
        this.namatoko = namatoko;
    }

    public String getNotelp() {
        return notelp;
    }

    public void setNotelp(String notelp) {
        this.notelp = notelp;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getDaerah() {
        return daerah;
    }

    public void setDaerah(String daerah) {
        this.daerah = daerah;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getKoordinat() {
        return koordinat;
    }

    public void setKoordinat(String koordinat) {
        this.koordinat = koordinat;
    }

    public String getEstimasi() {
        return estimasi;
    }

    public void setEstimasi(String estimasi) {
        this.estimasi = estimasi;
    }
}