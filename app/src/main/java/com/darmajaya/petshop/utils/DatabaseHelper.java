package com.darmajaya.petshop.utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;



public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DBNAME="petshop.sqlite";
    public static final String DBLOCATION="/data/data/com.darmajaya.petshop/databases/";
    private static final int DATABASE_VERSION = 1;
    private Context mContext;
    private SQLiteDatabase mDatabase;


    public void createDataBase() throws IOException {
        if(DataBaseisExist()){
            //do nothing - database already exist
            //Toast.makeText(myContext, "Database Sudah Ada", Toast.LENGTH_SHORT).show();
        }
        else{
            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            this.getReadableDatabase();

            try {
                copyDataBase();
                //Toast.makeText(myContext, "Database Berhasil Diimport Dari Assets", Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }

    }

    private boolean DataBaseisExist(){
        SQLiteDatabase checkDB = null;
        try{
            String myPath = DBLOCATION + DBNAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

        }catch(SQLiteException e){
            //database does't exist yet.
        }
        if(checkDB != null){
            checkDB.close();
        }
        if(checkDB != null )return true ;else return false;
    }

    private void copyDataBase() throws IOException{
        //Open your local db as the input stream
        InputStream myInput = mContext.getAssets().open(DBNAME);
        // Path to the just created empty db
        String outFileName = DBLOCATION + DBNAME;
        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);
        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }
        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    public DatabaseHelper(Context context){
        super(context,DBNAME,null,1);
        this.mContext=context;
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }
    
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
    
    public void openDatabase(){
        String dbPath=mContext.getDatabasePath(DBNAME).getPath();
        if(mDatabase!=null && mDatabase.isOpen()){
            return;
        }
        mDatabase=SQLiteDatabase.openDatabase(dbPath,null,SQLiteDatabase.OPEN_READWRITE);
    }
    
    public void closeDatabase(){
        if(mDatabase !=null){
            mDatabase.close();
        }
    }

    public List<TokoModel> getListWord(String wordSearch){
        TokoModel dictionaryModel=null;
        List<TokoModel> dictionaryModelList=new ArrayList<>();
        openDatabase();
        String[] args={"%"+wordSearch+"%"};

        Cursor cursor=mDatabase.rawQuery("Select * From tbltoko Where namatoko Like ? order by namatoko asc",args);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            dictionaryModel=new TokoModel(cursor.getString(0),cursor.getString(1),cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6));
            dictionaryModelList.add(dictionaryModel);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return dictionaryModelList;
    }



    public List<Toko> getkalianda() {
        Toko tempat = null;
        List<Toko> itemListpeta = new ArrayList<>();
        openDatabase();

        Cursor cursor = mDatabase.rawQuery("select * from tbltoko where daerah = \"Kalianda\"", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            tempat = new Toko(cursor.getString(0), cursor.getString(1), cursor.getString(2),
                    cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8));
            itemListpeta.add(tempat);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return itemListpeta;
    }

    public List<Toko> getpringsewu() {
        Toko tempat = null;
        List<Toko> itemListpeta = new ArrayList<>();
        openDatabase();

        Cursor cursor = mDatabase.rawQuery("select * from tbltoko where daerah = \"Pringsewu\"", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            tempat = new Toko(cursor.getString(0), cursor.getString(1), cursor.getString(2),
                    cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8));
            itemListpeta.add(tempat);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return itemListpeta;
    }

    public List<Toko> getbandarlampung() {
        Toko tempat = null;
        List<Toko> itemListpeta = new ArrayList<>();
        openDatabase();

        Cursor cursor = mDatabase.rawQuery("select * from tbltoko where daerah = \"Bandar Lampung\"", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            tempat = new Toko(cursor.getString(0), cursor.getString(1), cursor.getString(2),
                    cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8));
            itemListpeta.add(tempat);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return itemListpeta;
    }

    public List<Toko> getmetro() {
        Toko tempat = null;
        List<Toko> itemListpeta = new ArrayList<>();
        openDatabase();

        Cursor cursor = mDatabase.rawQuery("select * from tbltoko where daerah = \"Metro\"", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            tempat = new Toko(cursor.getString(0), cursor.getString(1), cursor.getString(2),
                    cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8));
            itemListpeta.add(tempat);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return itemListpeta;
    }

    public List<TokoModel> getall() {
        TokoModel tempat = null;
        List<TokoModel> itemListpeta = new ArrayList<>();
        openDatabase();

        Cursor cursor = mDatabase.rawQuery("select * from tbltoko", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            tempat = new TokoModel(cursor.getString(0), cursor.getString(1), cursor.getString(2),
                    cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6));
            itemListpeta.add(tempat);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return itemListpeta;
    }

}
