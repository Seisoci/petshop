package com.darmajaya.petshop.utils;

public class PetShop {
    private String namapetshop;
    private String notelpps;
    private String alamatps;
    private String deskripsi;
    private String nameuser;
    private String notelpuser;
    private String useremail;

    public PetShop() {

    }

    public PetShop(String namapetshop, String notelpps, String alamatps, String deskripsi, String nameuser, String notelpuser, String useremail) {
        this.namapetshop = namapetshop;
        this.notelpps = notelpps;
        this.alamatps = alamatps;
        this.deskripsi = deskripsi;
        this.nameuser = nameuser;
        this.notelpuser = notelpuser;
        this.useremail = useremail;
    }




    public String getNamapetshop() {
        return namapetshop;
    }

    public void setNamapetshop(String namapetshop) {
        this.namapetshop = namapetshop;
    }

    public String getNotelpps() {
        return notelpps;
    }

    public void setNotelpps(String notelpps) {
        this.notelpps = notelpps;
    }

    public String getAlamatps() {
        return alamatps;
    }

    public void setAlamatps(String alamatps) {
        this.alamatps = alamatps;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getNameuser() {
        return nameuser;
    }

    public void setNameuser(String nameuser) {
        this.nameuser = nameuser;
    }

    public String getNotelpuser() {
        return notelpuser;
    }

    public void setNotelpuser(String notelpuser) {
        this.notelpuser = notelpuser;
    }

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }
}
