package com.darmajaya.petshop;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;

/**
 * Created by Kokoro on 12/21/2017.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.darmajaya.petshop.adapters.Word_Adapter;
import com.darmajaya.petshop.utils.DatabaseHelper;
import com.darmajaya.petshop.utils.Toko;
import com.darmajaya.petshop.utils.TokoModel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;



public class TokoActivity extends AppCompatActivity {

    Button close;
    private RecyclerView rvWord;
    private Word_Adapter word_adapter;
    private List<TokoModel> dictionaryModelList;
    private DatabaseHelper mDBHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toko_activity);

        rvWord=(RecyclerView) findViewById(R.id.rvWord);
        rvWord.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        RecyclerView.ItemDecoration itemDecoration=new DividerItemDecoration(getApplicationContext(),DividerItemDecoration.VERTICAL);
        rvWord.addItemDecoration(itemDecoration);

        mDBHelper=new DatabaseHelper(this);

        dictionaryModelList=mDBHelper.getListWord("");

        word_adapter=new Word_Adapter();
        word_adapter.setData(dictionaryModelList);
        rvWord.setAdapter(word_adapter);

        SearchView searchView=(SearchView) findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchWord(newText);
                return false;
            }
        });



    }

    private void searchWord(String wordSearch){
        dictionaryModelList.clear();
        dictionaryModelList=mDBHelper.getListWord(wordSearch);
        word_adapter.setData(dictionaryModelList);
        rvWord.setAdapter(word_adapter);
    }


}

